/* window.rs
 *
 * Copyright 2023 Ondrej
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
use crate::helpers;

use gtk::prelude::*;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib, gdk};

use gdk::Display;

use async_channel;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/cz/ondrejkolin/Barcoder/window.ui")]
    pub struct BarcoderWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub entry: TemplateChild<adw::EntryRow>,

        #[template_child]
        pub picture: TemplateChild<gtk::Picture>,

        #[template_child]
        pub barcode_types: TemplateChild<adw::ComboRow>,

        #[template_child]
        pub error_row: TemplateChild<adw::ActionRow>,

        #[template_child]
        pub picture_actions: TemplateChild<gtk::Box>,

        #[template_child]
        pub save: TemplateChild<gtk::Button>
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BarcoderWindow {
        const NAME: &'static str = "BarcoderWindow";
        type Type = super::BarcoderWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BarcoderWindow {}
    impl WidgetImpl for BarcoderWindow {}
    impl WindowImpl for BarcoderWindow {}
    impl ApplicationWindowImpl for BarcoderWindow {}
    impl AdwApplicationWindowImpl for BarcoderWindow {}
}

glib::wrapper! {
    pub struct BarcoderWindow(ObjectSubclass<imp::BarcoderWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,        @implements gio::ActionGroup, gio::ActionMap;
}

impl BarcoderWindow {
    fn update_barcode(self: Self) {
            let window_impl = self.imp();
            let text = window_impl.entry.text();

            let selected = window_impl.barcode_types.selected_item().unwrap().downcast::<gtk::StringObject>().unwrap();
            if text.len() > 0 {
                let request = helpers::BarcodeRequest::new(text.as_str(), selected.string().as_str());
                let (sender, receiver) = async_channel::bounded(1);
                gio::spawn_blocking(move || {
                    let sender = sender.clone();
                    let barcode_generation = request.to_gdk_texture();
                    sender.send_blocking(barcode_generation).expect("Channel stuck");
                });
                glib::spawn_future_local(glib::clone!(@weak window_impl => async move {
                    let result: Result<gdk::Texture, rxing::Exceptions> = receiver.recv().await.expect("Unable to retrieve the value");
                    match result {
                        Ok(texture) => {
                            window_impl.error_row.set_visible(false);
                            window_impl.picture.set_paintable(Some(&texture));
                            window_impl.picture.set_visible(true);
                            window_impl.picture_actions.set_visible(true);
                        },
                        Err(err) => {
                            window_impl.error_row.set_visible(true);
                            window_impl.error_row.set_subtitle(format!("{}", err).as_str());
                            window_impl.picture.set_visible(false);
                            window_impl.picture_actions.set_visible(false);
                        }
                    };
                }));
            }
            else {
                window_impl.picture.set_visible(false);
                window_impl.picture_actions.set_visible(false);
            }
    }

    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        // Load the CSS
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/cz/ondrejkolin/Barcoder/main.css");
        gtk::style_context_add_provider_for_display(&Display::default().expect("Could not connect to a display."),&provider, gtk::STYLE_PROVIDER_PRIORITY_APPLICATION);

        let window = glib::Object::builder::<BarcoderWindow>()
            .property("application", application)
            .build();


        let model = gtk::StringList::new(&[]);
        helpers::BARCODES_PROCESORS.keys().for_each(|key| {
            model.append(key);
        });

        let imp = window.imp();

        imp.barcode_types.set_model(Some(&model));
        imp.barcode_types.connect_selected_notify(glib::clone!( @weak window => move |_| window.update_barcode() ));
        imp.entry.connect_changed(glib::clone!( @weak window => move |_| window.update_barcode() ));
        imp.save.connect_clicked(glib::clone!( @weak window => move |_| {
            let imp = window.imp();
            let fd = gtk::FileDialog::builder().title("Select the File for the Barcode").initial_name(format!("barcode_{}.png", imp.entry.text())).build();
            fd.save(Some(&window), None::<&gio::Cancellable>, glib::clone!(@weak window => move |file_response| {
                    let imp = window.imp();
                    if let Ok(response) = file_response {
                    let fp = response.replace_readwrite(
                        None,
                        false,
                        gio::FileCreateFlags::REPLACE_DESTINATION,
                        None::<&gio::Cancellable>,
                    ).unwrap();
                    let stream = fp.output_stream();
                    let bytes = imp.picture.paintable().unwrap().downcast::<gdk::Texture>().unwrap().save_to_png_bytes();
                    stream.write_bytes(&bytes, None::<&gio::Cancellable>).unwrap();
                }
            }));
        } ));

        window
    }
}

