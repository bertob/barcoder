use std::collections::HashMap;
use lazy_static::lazy_static;
use rxing::{BarcodeFormat, Writer};
use gtk::{glib, gdk};
use std::io::{self, Read};

use image;


lazy_static! {
        pub static ref BARCODES_PROCESORS: HashMap<String, BarcodeFormat> = {
            let mut map = HashMap::new();
            map.insert("EAN-13".to_string(), BarcodeFormat::EAN_13);
            map.insert("EAN-8".to_string(), BarcodeFormat::EAN_8);
            map.insert("CODE-128".to_string(), BarcodeFormat::CODE_128);
            map.insert("CODE-93".to_string(), BarcodeFormat::CODE_93);
            map.insert("CODE-39".to_string(), BarcodeFormat::CODE_39);
            map.insert("UPC-A".to_string(), BarcodeFormat::UPC_A);
            map.insert("UPC-E".to_string(), BarcodeFormat::UPC_E);
            map
        };
}

pub struct BarcodeRequest {
    text: String,
    barcode: String
}

impl BarcodeRequest {
    pub fn new(text: &str, barcode_type: &str) -> Self {
        Self {
            text: text.to_string(),
            barcode: barcode_type.to_string()
        }
    }

    pub fn to_gdk_texture(&self) -> Result<gdk::Texture, rxing::Exceptions> {
        let writer = rxing::MultiFormatWriter;
        let format = BARCODES_PROCESORS.get(&self.barcode).unwrap();
        let bit_matrix = writer.encode(self.text.as_str(), format, 400, 100)?;
        let mut c = io::Cursor::new(Vec::new());
        let img: image::DynamicImage = bit_matrix.into();
        img.write_to(&mut c, image::ImageOutputFormat::Png).unwrap();
        let mut c = io::Cursor::new(Vec::new());
        img.write_to(&mut c, image::ImageOutputFormat::Png).unwrap();
        c.set_position(0);
        let mut out = Vec::new();
        c.read_to_end(&mut out).unwrap();
        let glib_bytes = glib::Bytes::from_owned(out);
        Ok(gdk::Texture::from_bytes(&glib_bytes).unwrap())
    }
}

